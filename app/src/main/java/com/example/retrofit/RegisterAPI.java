package com.example.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterAPI {
    @FormUrlEncoded
    @POST("/insert")
    Call<Value> PRODUCT_CALL(
            @Field("nama") String nama,
            @Field("deskripsi") String deskripsi,
            @Field("harga") String harga);


}
